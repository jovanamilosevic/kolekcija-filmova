﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MovieCollection
{
    public partial class Form3 : Form
    {
        public Form3()
        {
            InitializeComponent();
        }
        static string myconstring = ConfigurationManager.ConnectionStrings["connstring"].ConnectionString;
        private void button1PretraziPoZanru_Click(object sender, EventArgs e)
        {
            string zanr = textBox1PretraziPoZanru.Text;

            SqlConnection konekcijaSaBazom = new SqlConnection(myconstring);
            SqlDataAdapter sda = new SqlDataAdapter("SELECT * FROM tbl_film WHERE zanr LIKE '%" + zanr+"%'", konekcijaSaBazom);
            DataTable dt = new DataTable();
            sda.Fill(dt);
            dataGridView1.DataSource = dt;
        }
    }
}
