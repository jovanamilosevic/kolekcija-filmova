﻿
namespace MovieCollection
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.button1KolekcijaFilmovaF1 = new System.Windows.Forms.Button();
            this.button2PretreziFilmovePoZanruF1 = new System.Windows.Forms.Button();
            this.button3PretraziFilmovePoGlumcuF1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // button1KolekcijaFilmovaF1
            // 
            this.button1KolekcijaFilmovaF1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(178)))), ((int)(((byte)(137)))));
            this.button1KolekcijaFilmovaF1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1KolekcijaFilmovaF1.ForeColor = System.Drawing.Color.White;
            this.button1KolekcijaFilmovaF1.Location = new System.Drawing.Point(62, 55);
            this.button1KolekcijaFilmovaF1.Name = "button1KolekcijaFilmovaF1";
            this.button1KolekcijaFilmovaF1.Size = new System.Drawing.Size(510, 53);
            this.button1KolekcijaFilmovaF1.TabIndex = 0;
            this.button1KolekcijaFilmovaF1.Text = "KOLEKCIJA FILMOVA";
            this.button1KolekcijaFilmovaF1.UseVisualStyleBackColor = false;
            this.button1KolekcijaFilmovaF1.Click += new System.EventHandler(this.button1KolekcijaFilmova_Click);
            // 
            // button2PretreziFilmovePoZanruF1
            // 
            this.button2PretreziFilmovePoZanruF1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(178)))), ((int)(((byte)(137)))));
            this.button2PretreziFilmovePoZanruF1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2PretreziFilmovePoZanruF1.ForeColor = System.Drawing.Color.White;
            this.button2PretreziFilmovePoZanruF1.Location = new System.Drawing.Point(62, 144);
            this.button2PretreziFilmovePoZanruF1.Name = "button2PretreziFilmovePoZanruF1";
            this.button2PretreziFilmovePoZanruF1.Size = new System.Drawing.Size(510, 54);
            this.button2PretreziFilmovePoZanruF1.TabIndex = 1;
            this.button2PretreziFilmovePoZanruF1.Text = "PRETRAŽI FILMOVE PO ŽANRU";
            this.button2PretreziFilmovePoZanruF1.UseVisualStyleBackColor = false;
            this.button2PretreziFilmovePoZanruF1.Click += new System.EventHandler(this.button2PretreziFilmovePoZanruF1_Click);
            // 
            // button3PretraziFilmovePoGlumcuF1
            // 
            this.button3PretraziFilmovePoGlumcuF1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(178)))), ((int)(((byte)(137)))));
            this.button3PretraziFilmovePoGlumcuF1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3PretraziFilmovePoGlumcuF1.ForeColor = System.Drawing.Color.White;
            this.button3PretraziFilmovePoGlumcuF1.Location = new System.Drawing.Point(62, 234);
            this.button3PretraziFilmovePoGlumcuF1.Name = "button3PretraziFilmovePoGlumcuF1";
            this.button3PretraziFilmovePoGlumcuF1.Size = new System.Drawing.Size(510, 53);
            this.button3PretraziFilmovePoGlumcuF1.TabIndex = 2;
            this.button3PretraziFilmovePoGlumcuF1.Text = "PRETRAŽI FILMOVE PO GLAVNOM GLUMCU";
            this.button3PretraziFilmovePoGlumcuF1.UseVisualStyleBackColor = false;
            this.button3PretraziFilmovePoGlumcuF1.Click += new System.EventHandler(this.button3PretraziFilmovePoGlumcuF1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(51)))), ((int)(((byte)(60)))));
            this.ClientSize = new System.Drawing.Size(628, 344);
            this.Controls.Add(this.button3PretraziFilmovePoGlumcuF1);
            this.Controls.Add(this.button2PretreziFilmovePoZanruF1);
            this.Controls.Add(this.button1KolekcijaFilmovaF1);
            this.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "KOLEKCIJA FILMOVA";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button1KolekcijaFilmovaF1;
        private System.Windows.Forms.Button button2PretreziFilmovePoZanruF1;
        private System.Windows.Forms.Button button3PretraziFilmovePoGlumcuF1;
    }
}

