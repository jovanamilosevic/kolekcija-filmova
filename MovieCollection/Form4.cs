﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MovieCollection
{
    public partial class Form4 : Form
    {
        public Form4()
        {
            InitializeComponent();
        }
        static string myconstring = ConfigurationManager.ConnectionStrings["connstring"].ConnectionString;
        private void button1PretraziPoGlumcu_Click(object sender, EventArgs e)
        {
            string glavniGlumac = textBox1PretraziPoGlumcu.Text;

            SqlConnection konekcijaSaBazom = new SqlConnection(myconstring);
            SqlDataAdapter sda = new SqlDataAdapter("SELECT * FROM tbl_film WHERE glavniGlumac LIKE '%" + glavniGlumac + "%'", konekcijaSaBazom);
            DataTable dt = new DataTable();
            sda.Fill(dt);
            dataGridView1.DataSource = dt;
        }
    }
}
