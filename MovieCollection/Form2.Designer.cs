﻿
namespace MovieCollection
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form2));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.button1Ocisti = new System.Windows.Forms.Button();
            this.button1Dodaj = new System.Windows.Forms.Button();
            this.textBox5Opis = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.textBox6GlavniGlumac = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.textBox3Trajanje = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBox4Reziser = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.comboBox1Zanr = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBox2Naziv = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox1ID = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dataGridView1KolekcijaFilmova = new System.Windows.Forms.DataGridView();
            this.button5Azuriraj = new System.Windows.Forms.Button();
            this.button4Obrisi = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1KolekcijaFilmova)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.button1Ocisti);
            this.groupBox1.Controls.Add(this.button1Dodaj);
            this.groupBox1.Controls.Add(this.textBox5Opis);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.textBox6GlavniGlumac);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.textBox3Trajanje);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.textBox4Reziser);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.comboBox1Zanr);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.textBox2Naziv);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.textBox1ID);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(543, 529);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // button1Ocisti
            // 
            this.button1Ocisti.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(178)))), ((int)(((byte)(137)))));
            this.button1Ocisti.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1Ocisti.ForeColor = System.Drawing.Color.White;
            this.button1Ocisti.Location = new System.Drawing.Point(328, 413);
            this.button1Ocisti.Name = "button1Ocisti";
            this.button1Ocisti.Size = new System.Drawing.Size(202, 48);
            this.button1Ocisti.TabIndex = 15;
            this.button1Ocisti.Text = "Očisti";
            this.button1Ocisti.UseVisualStyleBackColor = false;
            this.button1Ocisti.Click += new System.EventHandler(this.button1Ocisti_Click);
            // 
            // button1Dodaj
            // 
            this.button1Dodaj.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(178)))), ((int)(((byte)(137)))));
            this.button1Dodaj.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1Dodaj.ForeColor = System.Drawing.Color.White;
            this.button1Dodaj.Location = new System.Drawing.Point(328, 475);
            this.button1Dodaj.Name = "button1Dodaj";
            this.button1Dodaj.Size = new System.Drawing.Size(202, 48);
            this.button1Dodaj.TabIndex = 14;
            this.button1Dodaj.Text = "Dodaj";
            this.button1Dodaj.UseVisualStyleBackColor = false;
            this.button1Dodaj.Click += new System.EventHandler(this.button1Dodaj_Click);
            // 
            // textBox5Opis
            // 
            this.textBox5Opis.BackColor = System.Drawing.SystemColors.Window;
            this.textBox5Opis.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox5Opis.Location = new System.Drawing.Point(10, 450);
            this.textBox5Opis.Multiline = true;
            this.textBox5Opis.Name = "textBox5Opis";
            this.textBox5Opis.Size = new System.Drawing.Size(300, 73);
            this.textBox5Opis.TabIndex = 13;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(6, 425);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(61, 24);
            this.label6.TabIndex = 12;
            this.label6.Text = "Opis:";
            // 
            // textBox6GlavniGlumac
            // 
            this.textBox6GlavniGlumac.BackColor = System.Drawing.SystemColors.Window;
            this.textBox6GlavniGlumac.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox6GlavniGlumac.Location = new System.Drawing.Point(10, 381);
            this.textBox6GlavniGlumac.Multiline = true;
            this.textBox6GlavniGlumac.Name = "textBox6GlavniGlumac";
            this.textBox6GlavniGlumac.Size = new System.Drawing.Size(300, 35);
            this.textBox6GlavniGlumac.TabIndex = 11;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(6, 356);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(151, 24);
            this.label7.TabIndex = 10;
            this.label7.Text = "Glavni glumac:";
            // 
            // textBox3Trajanje
            // 
            this.textBox3Trajanje.BackColor = System.Drawing.SystemColors.Window;
            this.textBox3Trajanje.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox3Trajanje.Location = new System.Drawing.Point(10, 312);
            this.textBox3Trajanje.Multiline = true;
            this.textBox3Trajanje.Name = "textBox3Trajanje";
            this.textBox3Trajanje.Size = new System.Drawing.Size(300, 35);
            this.textBox3Trajanje.TabIndex = 9;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(6, 287);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(92, 24);
            this.label4.TabIndex = 8;
            this.label4.Text = "Trajanje:";
            // 
            // textBox4Reziser
            // 
            this.textBox4Reziser.BackColor = System.Drawing.SystemColors.Window;
            this.textBox4Reziser.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox4Reziser.Location = new System.Drawing.Point(10, 243);
            this.textBox4Reziser.Multiline = true;
            this.textBox4Reziser.Name = "textBox4Reziser";
            this.textBox4Reziser.Size = new System.Drawing.Size(300, 35);
            this.textBox4Reziser.TabIndex = 7;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(6, 218);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(87, 24);
            this.label5.TabIndex = 6;
            this.label5.Text = "Režiser:";
            // 
            // comboBox1Zanr
            // 
            this.comboBox1Zanr.BackColor = System.Drawing.SystemColors.Window;
            this.comboBox1Zanr.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox1Zanr.FormattingEnabled = true;
            this.comboBox1Zanr.ItemHeight = 24;
            this.comboBox1Zanr.Items.AddRange(new object[] {
            "Akcioni",
            "Misterija",
            "Horor",
            "Romantični",
            "Naučna fantastika",
            "Krimi",
            "Drama",
            "Komedija"});
            this.comboBox1Zanr.Location = new System.Drawing.Point(10, 181);
            this.comboBox1Zanr.Name = "comboBox1Zanr";
            this.comboBox1Zanr.Size = new System.Drawing.Size(300, 32);
            this.comboBox1Zanr.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(6, 156);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 24);
            this.label3.TabIndex = 4;
            this.label3.Text = "Žanr:";
            // 
            // textBox2Naziv
            // 
            this.textBox2Naziv.BackColor = System.Drawing.SystemColors.Window;
            this.textBox2Naziv.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox2Naziv.Location = new System.Drawing.Point(10, 112);
            this.textBox2Naziv.Multiline = true;
            this.textBox2Naziv.Name = "textBox2Naziv";
            this.textBox2Naziv.Size = new System.Drawing.Size(300, 35);
            this.textBox2Naziv.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(6, 87);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(68, 24);
            this.label2.TabIndex = 2;
            this.label2.Text = "Naziv:";
            // 
            // textBox1ID
            // 
            this.textBox1ID.BackColor = System.Drawing.SystemColors.Window;
            this.textBox1ID.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1ID.Location = new System.Drawing.Point(10, 43);
            this.textBox1ID.Multiline = true;
            this.textBox1ID.Name = "textBox1ID";
            this.textBox1ID.ReadOnly = true;
            this.textBox1ID.Size = new System.Drawing.Size(300, 35);
            this.textBox1ID.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(6, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(36, 24);
            this.label1.TabIndex = 0;
            this.label1.Text = "ID:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.dataGridView1KolekcijaFilmova);
            this.groupBox2.Controls.Add(this.button5Azuriraj);
            this.groupBox2.Controls.Add(this.button4Obrisi);
            this.groupBox2.Location = new System.Drawing.Point(574, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(543, 529);
            this.groupBox2.TabIndex = 15;
            this.groupBox2.TabStop = false;
            // 
            // dataGridView1KolekcijaFilmova
            // 
            this.dataGridView1KolekcijaFilmova.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1KolekcijaFilmova.Location = new System.Drawing.Point(16, 19);
            this.dataGridView1KolekcijaFilmova.Name = "dataGridView1KolekcijaFilmova";
            this.dataGridView1KolekcijaFilmova.RowHeadersWidth = 51;
            this.dataGridView1KolekcijaFilmova.RowTemplate.Height = 24;
            this.dataGridView1KolekcijaFilmova.Size = new System.Drawing.Size(515, 388);
            this.dataGridView1KolekcijaFilmova.TabIndex = 19;
            this.dataGridView1KolekcijaFilmova.RowHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridView1KolekcijaFilmova_RowHeaderMouseClick);
            // 
            // button5Azuriraj
            // 
            this.button5Azuriraj.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(178)))), ((int)(((byte)(137)))));
            this.button5Azuriraj.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button5Azuriraj.ForeColor = System.Drawing.Color.White;
            this.button5Azuriraj.Location = new System.Drawing.Point(183, 413);
            this.button5Azuriraj.Name = "button5Azuriraj";
            this.button5Azuriraj.Size = new System.Drawing.Size(202, 48);
            this.button5Azuriraj.TabIndex = 18;
            this.button5Azuriraj.Text = "Ažuriraj";
            this.button5Azuriraj.UseVisualStyleBackColor = false;
            this.button5Azuriraj.Click += new System.EventHandler(this.button5Azuriraj_Click);
            // 
            // button4Obrisi
            // 
            this.button4Obrisi.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(131)))), ((int)(((byte)(132)))));
            this.button4Obrisi.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4Obrisi.ForeColor = System.Drawing.Color.White;
            this.button4Obrisi.Location = new System.Drawing.Point(183, 475);
            this.button4Obrisi.Name = "button4Obrisi";
            this.button4Obrisi.Size = new System.Drawing.Size(202, 48);
            this.button4Obrisi.TabIndex = 17;
            this.button4Obrisi.Text = "Obriši";
            this.button4Obrisi.UseVisualStyleBackColor = false;
            this.button4Obrisi.Click += new System.EventHandler(this.button4Obrisi_Click);
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(51)))), ((int)(((byte)(60)))));
            this.ClientSize = new System.Drawing.Size(1130, 553);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form2";
            this.Text = "KOLEKCIJA FILMOVA";
            this.Load += new System.EventHandler(this.Form2_load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1KolekcijaFilmova)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox comboBox1Zanr;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBox2Naziv;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox1ID;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1Dodaj;
        private System.Windows.Forms.TextBox textBox5Opis;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBox6GlavniGlumac;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBox3Trajanje;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBox4Reziser;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button button5Azuriraj;
        private System.Windows.Forms.Button button4Obrisi;
        private System.Windows.Forms.DataGridView dataGridView1KolekcijaFilmova;
        private System.Windows.Forms.Button button1Ocisti;
    }
}