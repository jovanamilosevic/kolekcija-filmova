﻿
namespace MovieCollection
{
    partial class Form4
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form4));
            this.button1PretraziPoGlumcu = new System.Windows.Forms.Button();
            this.textBox1PretraziPoGlumcu = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // button1PretraziPoGlumcu
            // 
            this.button1PretraziPoGlumcu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(178)))), ((int)(((byte)(137)))));
            this.button1PretraziPoGlumcu.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1PretraziPoGlumcu.ForeColor = System.Drawing.Color.White;
            this.button1PretraziPoGlumcu.Location = new System.Drawing.Point(208, 139);
            this.button1PretraziPoGlumcu.Name = "button1PretraziPoGlumcu";
            this.button1PretraziPoGlumcu.Size = new System.Drawing.Size(155, 42);
            this.button1PretraziPoGlumcu.TabIndex = 6;
            this.button1PretraziPoGlumcu.Text = "Pretraži";
            this.button1PretraziPoGlumcu.UseVisualStyleBackColor = false;
            this.button1PretraziPoGlumcu.Click += new System.EventHandler(this.button1PretraziPoGlumcu_Click);
            // 
            // textBox1PretraziPoGlumcu
            // 
            this.textBox1PretraziPoGlumcu.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1PretraziPoGlumcu.Location = new System.Drawing.Point(154, 78);
            this.textBox1PretraziPoGlumcu.Multiline = true;
            this.textBox1PretraziPoGlumcu.Name = "textBox1PretraziPoGlumcu";
            this.textBox1PretraziPoGlumcu.Size = new System.Drawing.Size(284, 36);
            this.textBox1PretraziPoGlumcu.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(41, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(550, 29);
            this.label1.TabIndex = 4;
            this.label1.Text = "Pretražite kolekciju filmova po glavnm glumcu:";
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(12, 204);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersWidth = 51;
            this.dataGridView1.RowTemplate.Height = 24;
            this.dataGridView1.Size = new System.Drawing.Size(598, 234);
            this.dataGridView1.TabIndex = 7;
            // 
            // Form4
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(51)))), ((int)(((byte)(60)))));
            this.ClientSize = new System.Drawing.Size(622, 450);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.button1PretraziPoGlumcu);
            this.Controls.Add(this.textBox1PretraziPoGlumcu);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form4";
            this.Text = "PRETRAGA PO GLAVNOM GLUMCU";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button button1PretraziPoGlumcu;
        private System.Windows.Forms.TextBox textBox1PretraziPoGlumcu;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dataGridView1;
    }
}