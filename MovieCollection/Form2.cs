﻿using MovieCollection.kolekcijaFilmovaKlase;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MovieCollection
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }
        kolekcijaFilmova k = new kolekcijaFilmova();

        private void button2_Click(object sender, EventArgs e)
        {

        }

        private void button1Dodaj_Click(object sender, EventArgs e)
        {
            k.naziv = textBox2Naziv.Text;
            k.zanr = comboBox1Zanr.Text;
            k.reziser = textBox4Reziser.Text;
            k.trajanje = Convert.ToSingle(textBox3Trajanje.Text);
            k.glavniGlumac = textBox6GlavniGlumac.Text;
            k.opis = textBox5Opis.Text;

            bool uspesnoDodato = k.UnosenjePodatakaUBazu(k);
            if (uspesnoDodato == true)
            {
                MessageBox.Show("Uspešno ste uneli podatke o filmu!!!");
                OcistiPodatkeSaForme();
            }
            else
            {
                MessageBox.Show("Došlo je do greške pri unosu filma. Pokušajte ponovo ");
                
            }

            DataTable dt = k.Select();
            dataGridView1KolekcijaFilmova.DataSource = dt;
        }
        private void Form2_load(object sender, EventArgs e)
        {

            DataTable dt = k.Select();
            dataGridView1KolekcijaFilmova.DataSource = dt;
        }
        public void OcistiPodatkeSaForme()
        {
            textBox1ID.Text = "";
            textBox2Naziv.Text="";
            textBox3Trajanje.Text = "";
            textBox4Reziser.Text = "";
            textBox5Opis.Text = "";
            textBox6GlavniGlumac.Text = "";
            comboBox1Zanr.Text = "";


        }

        private void button5Azuriraj_Click(object sender, EventArgs e)
        {
            k.ID = Convert.ToInt32(textBox1ID.Text);
            k.naziv = textBox2Naziv.Text;
            k.zanr = comboBox1Zanr.Text;
            k.reziser = textBox4Reziser.Text;
            k.trajanje = Convert.ToSingle(textBox3Trajanje.Text);
            k.glavniGlumac = textBox6GlavniGlumac.Text;
            k.opis = textBox5Opis.Text;

            bool uspesnoAzurirano = k.Azuriraj(k);
            if (uspesnoAzurirano == true)
            {
                MessageBox.Show("Uspešno ste ažurirali podatke o filmu!!!");
                OcistiPodatkeSaForme();

                DataTable dt = k.Select();
                dataGridView1KolekcijaFilmova.DataSource = dt;
            }
            else
            {
                MessageBox.Show("Došlo je do greške pri ažuriranju filma. Pokušajte ponovo ");

            }

        }

        private void dataGridView1KolekcijaFilmova_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            int indexReda = e.RowIndex;
            textBox1ID.Text = dataGridView1KolekcijaFilmova.Rows[indexReda].Cells[0].Value.ToString();
            textBox2Naziv.Text = dataGridView1KolekcijaFilmova.Rows[indexReda].Cells[1].Value.ToString();
            comboBox1Zanr.Text = dataGridView1KolekcijaFilmova.Rows[indexReda].Cells[2].Value.ToString();
            textBox4Reziser.Text = dataGridView1KolekcijaFilmova.Rows[indexReda].Cells[3].Value.ToString();
            textBox3Trajanje.Text = dataGridView1KolekcijaFilmova.Rows[indexReda].Cells[4].Value.ToString();
            textBox6GlavniGlumac.Text = dataGridView1KolekcijaFilmova.Rows[indexReda].Cells[5].Value.ToString();
            textBox5Opis.Text = dataGridView1KolekcijaFilmova.Rows[indexReda].Cells[6].Value.ToString();

        }

        private void button1Ocisti_Click(object sender, EventArgs e)
        {
            OcistiPodatkeSaForme();
        }

        private void button4Obrisi_Click(object sender, EventArgs e)
        {
            k.ID = Convert.ToInt32(textBox1ID.Text);
            bool uspesnoObrisano = k.Obrisi(k);
            if (uspesnoObrisano == true)
            {
                MessageBox.Show("Uspešno ste obrisali podatke o filmu!!!");
                OcistiPodatkeSaForme();

                DataTable dt = k.Select();
                dataGridView1KolekcijaFilmova.DataSource = dt;
            }
            else
            {
                MessageBox.Show("Došlo je do greške pri brisanju filma. Pokušajte ponovo ");

            }
        }
    }
}
