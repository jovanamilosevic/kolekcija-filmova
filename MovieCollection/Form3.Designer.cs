﻿
namespace MovieCollection
{
    partial class Form3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form3));
            this.label1 = new System.Windows.Forms.Label();
            this.textBox1PretraziPoZanru = new System.Windows.Forms.TextBox();
            this.button1PretraziPoZanru = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(37, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(437, 29);
            this.label1.TabIndex = 0;
            this.label1.Text = "Pretražite kolekciju filmova po žanru:";
            // 
            // textBox1PretraziPoZanru
            // 
            this.textBox1PretraziPoZanru.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1PretraziPoZanru.Location = new System.Drawing.Point(110, 87);
            this.textBox1PretraziPoZanru.Multiline = true;
            this.textBox1PretraziPoZanru.Name = "textBox1PretraziPoZanru";
            this.textBox1PretraziPoZanru.Size = new System.Drawing.Size(284, 36);
            this.textBox1PretraziPoZanru.TabIndex = 1;
            // 
            // button1PretraziPoZanru
            // 
            this.button1PretraziPoZanru.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(178)))), ((int)(((byte)(137)))));
            this.button1PretraziPoZanru.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1PretraziPoZanru.ForeColor = System.Drawing.Color.White;
            this.button1PretraziPoZanru.Location = new System.Drawing.Point(174, 146);
            this.button1PretraziPoZanru.Name = "button1PretraziPoZanru";
            this.button1PretraziPoZanru.Size = new System.Drawing.Size(155, 42);
            this.button1PretraziPoZanru.TabIndex = 2;
            this.button1PretraziPoZanru.Text = "Pretraži";
            this.button1PretraziPoZanru.UseVisualStyleBackColor = false;
            this.button1PretraziPoZanru.Click += new System.EventHandler(this.button1PretraziPoZanru_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(12, 214);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersWidth = 51;
            this.dataGridView1.RowTemplate.Height = 24;
            this.dataGridView1.Size = new System.Drawing.Size(500, 224);
            this.dataGridView1.TabIndex = 3;
            // 
            // Form3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(51)))), ((int)(((byte)(60)))));
            this.ClientSize = new System.Drawing.Size(524, 450);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.button1PretraziPoZanru);
            this.Controls.Add(this.textBox1PretraziPoZanru);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form3";
            this.Text = "PRETRAGA PO ŽANRU";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox1PretraziPoZanru;
        private System.Windows.Forms.Button button1PretraziPoZanru;
        private System.Windows.Forms.DataGridView dataGridView1;
    }
}