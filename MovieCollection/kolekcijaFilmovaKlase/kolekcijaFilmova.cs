﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieCollection.kolekcijaFilmovaKlase
{
    class kolekcijaFilmova
    {
        public int ID { get; set; }
        public string naziv { get; set; }
        public string zanr { get; set; }
        public string reziser { get; set; }
        public float trajanje { get; set; }
        public string glavniGlumac { get; set; }
        public string opis { get; set; }

        static string myconstring = ConfigurationManager.ConnectionStrings["connstring"].ConnectionString;

        public DataTable Select()
        {
            SqlConnection konekcijaSaBazom = new SqlConnection(myconstring);
            DataTable dt = new DataTable();
            try
            {
                string sql = "SELECT * from tbl_film";
                SqlCommand cmd = new SqlCommand(sql, konekcijaSaBazom);
                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                konekcijaSaBazom.Open();
                adapter.Fill(dt);

            }
            catch(Exception ex)
            {

            }
            finally
            {
                konekcijaSaBazom.Close();
            }
            return dt;
        }
        public bool UnosenjePodatakaUBazu(kolekcijaFilmova k)
        {
            bool uspesnoUneto = false;

            SqlConnection konekcijaSaBazom = new SqlConnection(myconstring);
            try
            {
                string sql = "INSERT INTO tbl_film (naziv, zanr, reziser, trajanje, glavniGlumac, opis) VALUES(@naziv, @zanr, @reziser, @trajanje, @glavniGlumac, @opis)";
                SqlCommand cmd = new SqlCommand(sql, konekcijaSaBazom);
                cmd.Parameters.AddWithValue("@naziv", k.naziv);
                cmd.Parameters.AddWithValue("@zanr", k.zanr);
                cmd.Parameters.AddWithValue("@reziser", k.reziser);
                cmd.Parameters.AddWithValue("@trajanje", k.trajanje);
                cmd.Parameters.AddWithValue("@glavniGlumac", k.glavniGlumac);
                cmd.Parameters.AddWithValue("@opis", k.opis);

                konekcijaSaBazom.Open();
                int red = cmd.ExecuteNonQuery();
                if (red > 0)
                {
                    uspesnoUneto = true;
                }
            }
            catch(Exception e)
            {

            }
            finally
            {
                konekcijaSaBazom.Close();
            }
            return uspesnoUneto;
        }
        public bool Azuriraj(kolekcijaFilmova k)
        {
            bool uspesnoAzurirano = false;
            SqlConnection konekcijaSaBazom = new SqlConnection(myconstring);
            try
            {
                string sql = "UPDATE tbl_film SET naziv=@naziv, zanr=@zanr, reziser=@reziser, trajanje=@trajanje, glavniGlumac=@glavniGlumac, opis=@opis WHERE ID=@ID";
                SqlCommand cmd = new SqlCommand(sql, konekcijaSaBazom);
                
                cmd.Parameters.AddWithValue("@naziv", k.naziv);
                cmd.Parameters.AddWithValue("@zanr", k.zanr);
                cmd.Parameters.AddWithValue("@reziser", k.reziser);
                cmd.Parameters.AddWithValue("@trajanje", k.trajanje);
                cmd.Parameters.AddWithValue("@glavniGlumac", k.glavniGlumac);
                cmd.Parameters.AddWithValue("@opis", k.opis);
                cmd.Parameters.AddWithValue("@ID", k.ID);

                konekcijaSaBazom.Open();
                int red = cmd.ExecuteNonQuery();

                if (red > 0)
                {
                    uspesnoAzurirano = true;
                }
                else
                {
                    uspesnoAzurirano = false;
                }
                    
            }
            catch(Exception e)
            {

            }
            finally
            {
                konekcijaSaBazom.Close();
            }

            return uspesnoAzurirano;
        }
        public bool Obrisi(kolekcijaFilmova k)
        {
            bool uspesnoObrisano = false;

            return uspesnoObrisano;
            SqlConnection konekcijaSaBazom = new SqlConnection(myconstring);

            try
            {
                string sql = "DELETE FROM tbl_film WHERE ID=@ID";
                SqlCommand cmd = new SqlCommand(sql, konekcijaSaBazom);
                cmd.Parameters.AddWithValue("@ID", k.ID);

                konekcijaSaBazom.Open();

                int red = cmd.ExecuteNonQuery();
                if (red > 0)
                {
                    uspesnoObrisano = true;
                }
                else
                {
                    uspesnoObrisano = false;
                }
            }
            catch(Exception e)
            {

            }
            finally
            {
                konekcijaSaBazom.Close();
            }
        }
    }
}
